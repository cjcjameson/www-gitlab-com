---
layout: handbook-page-toc
title: "E-Group Weekly"
description: "For executives to connect on a weekly basis in order to get timely input from E-Group, align on key initiatives, inform about key business happenings and celebrate company successes."
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Purpose

For [executives](/company/team/structure/#executives) to connect on a weekly basis in order to:

1. Get timely input from E-Group
1. Align on key initiatives
1. Inform about key business happenings 
1. Celebrate company successes

## Timing
Scheduled for 80 minutes on Mondays. These don't occur during 
[E-Group Offsite](/company/offsite/)
weeks and may occasionally be rescheduled for another day due to calendar conflicts or holidays. 

## Attendees
1. [Executives](/company/team/structure/#executives)
1. [Chief of Staff to the CEO](/job-families/chief-executive-officer/chief-of-staff/); when not possible, the [Director, Strategy and Operations](/job-families/chief-executive-officer/strategy-and-operations/)
1. [CEO Shadows](/handbook/ceo/shadow/)
1. [Executive Business Admin](/handbook//eba/#executive-business-administrator-team) to the CEO (optional)
1. Invited participants: folks invited to participate in one or more specific session
   1. The slack channel #e-group-waiting-room is used to ping participants if the start time changes

## Scheduling
The [EBA to the CEO](https://about.gitlab.com/handbook/eba/#executive-business-administrator-team) is responsible for scheduling these meetings. [Chief of Staff to the CEO](https://about.gitlab.com/job-families/chief-executive-officer/chief-of-staff/#directed-work) is responsible for managing to the agenda and ensuring that meetings don't exceed the allocated time. 

## Meeting Details
1. Before adding to the agenda, E-Group members should consider whether topics are best suited for the meeting agenda or can be covered as FYIs in Slack
1. All agenda items including any pre-read material should be added to the agenda by 5pm PT *two business days* before the meeting unless something unexpected happens between the end of normal submissions and when the meeting occurs. If something unexpected occurs after this time, please add the topic at the bottom of the agenda and flag to E-Group in the #e-group or #e-group-confidential Slack channels
1. Submissions should include how much time would ideally be allocated for a specific topic 
1. The [CoS to the CEO](/job-families/chief-executive-officer/chief-of-staff/) will organize agenda suggestions and allocate discussion times by 3pm PT on the business day immediately before the meeting
1. If pre-work is strongly encouraged, it should be flagged as early possible in the week before in #e-group or another appropriate Slack channel
1. If you are inviting external folks to a specific session, please notify the EBA to the CEO at least two business days before the meeting
1. Ensure that any invited participants are clear about their role in the discussion
1. The CoS to the CEO will manage time within the meeting

## Agenda structure
1. The agenda for each E-Group Weekly has three sections:
   1. Topics for discussion: items that require allocated time during the meeting
   1. FYIs: items that don't need any discussion time and can be reviewed async
   1. Department updates: a **brief** overview of key business happenings within the department. If an E-Group member has an update that will take **over one minute** or require team input, discussion items should instead be added to the "topics for discussion" section. In department updates, it is especially important to share:
      1. Items not otherwise covered in [Key Reviews](/handbook/key-review/) or [Group Conversations](/handbook/group-conversations/)--or items in these that deserve an additional spotlight
      1. Changes or updates that impact the business as a whole
      1. Activities that are of cross-functional interest
      1. Notable progress or lack of progress against goals
1. Each "Topics for discussion" agenda item should clearly state the objective for the topic. For example:
   1. Inform about upcoming announcement
   1. Get feedback on a proposal
   1. Make a decision on the Contribute location
